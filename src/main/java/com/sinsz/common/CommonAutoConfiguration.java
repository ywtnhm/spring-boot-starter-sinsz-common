package com.sinsz.common;

import com.sinsz.common.interceptor.LogsInterceptor;
import com.sinsz.common.properties.CommonProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 配置入口
 * @author chenjianbo
 */
@Configuration
@ComponentScans({
        @ComponentScan({"com.sinsz.common"})
})
@EnableConfigurationProperties(CommonProperties.class)
public class CommonAutoConfiguration implements WebMvcConfigurer {

    private final CommonProperties properties;

    @Autowired
    public CommonAutoConfiguration(CommonProperties properties) {
        this.properties = properties;
    }

    @Bean
    public HttpHeaders httpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        if (properties.getHeaders() != null
                && !properties.getHeaders().isEmpty()) {
            properties.getHeaders().forEach(headers::set);
        }
        return headers;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LogsInterceptor()).addPathPatterns("/**");
    }

}