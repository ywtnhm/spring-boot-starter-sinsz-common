package com.sinsz.common.exception;

/**
 * 默认错误代码
 * @author chenjianbo
 * @date 2018-11-30
 */
public enum DefaultError implements IError {

    /**
     * 系统异常
     */
    SYSTEM_ERR_01(-1, "系统异常"),

    /**
     * 未找到请求
     */
    NOT_FOUND_01(-2, "请求未找到"),

    /**
     * 签名异常
     */
    SIGN_ERR_01(-3, "参数签名异常"),

    /**
     * 签名不能为空
     */
    SIGN_ERR_02(-4, "签名不能为空"),

    /**
     * 请求表示不能为空
     */
    REQUEST_TAG_01(-5, "请求标识不能为空"),

    ;

    DefaultError(int errCode, String errMsg) {
        this.errCode = errCode;
        this.errMsg = errMsg;
    }

    private int errCode;

    private String errMsg;

    @Override
    public int errCode() {
        return this.errCode;
    }

    @Override
    public String errMsg() {
        return this.errMsg;
    }

}
