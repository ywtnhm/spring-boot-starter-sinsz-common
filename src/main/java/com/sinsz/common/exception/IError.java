package com.sinsz.common.exception;

/**
 * 错误类接口
 * @author chenjianbo
 * @date 2018-11-30
 */
public interface IError {

    /**
     * 错误代码
     * @return
     */
    int errCode();

    /**
     * 错误消息内容
     * @return
     */
    String errMsg();

}
