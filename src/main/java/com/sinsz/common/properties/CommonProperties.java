package com.sinsz.common.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import java.util.Map;

/**
 * @author chenjianbo
 * @date 2018-11-30
 */
@Validated
@ConfigurationProperties("sinsz.common")
public class CommonProperties {

    /**
     * 请求头信息记录
     */
    private Map<String, String> headers;

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

}
