package com.sinsz.common.util;

/**
 * 签名加密类型
 * @author chenjianbo
 * @date 2018-12-3
 */
public enum SignEnum {

    MD5,

    SHA1;


    private String source;

    public SignEnum setSource(String source) {
        this.source = source;
        return this;
    }

    @Override
    public String toString() {
        switch (this) {
            case MD5: return SignUtils.signatureMd5(this.source).toUpperCase();
            case SHA1: return SignUtils.signatureSha1(this.source).toUpperCase();
            default:
                return "";
        }
    }
}
