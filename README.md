# spring-boot-starter-sinsz-common

#### 项目介绍
        基于springboot扩展公共依赖

        Copyright © 2018 sinsz.com All rights reserved.

        https://www.sinsz.com
` 最新版本：0.0.1 `

#### 安装教程
maven方式:
```
<dependency>
    <groupId>com.sinsz</groupId>
    <artifactId>spring-boot-starter-sinsz-common</artifactId>
    <version>0.0.1</version>
</dependency>
```
gradle方式:
```
compile group: 'com.sinsz', name: 'spring-boot-starter-sinsz-common', version: '0.0.1'
```

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request